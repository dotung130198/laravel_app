<?php


use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::name('products.')->prefix('products')
    ->group(function(){
        Route::get('/',[ProductController::class,'index'])->name('index');
        Route::post('/',[ProductController::class,'store'])->name('store')
            ->middleware('auth');
        Route::get('/create',[ProductController::class,'create'])->name('create');
        Route::get('/{id}',[ProductController::class,'show'])->name('show');
        Route::delete('/{id}',[ProductController::class,'destroy'])->name('delete')
            ->middleware('auth');
        Route::get('/{id}/edit',[ProductController::class,'edit'])->name('edit')
            ->middleware('auth');
        Route::put('/{id}',[ProductController::class,'update'])->name('update')
            ->middleware('auth');
    });

Route::name('users.')->prefix('users')
    ->group(function(){
        Route::get('/',[UserController::class,'index'])->name('index');
        Route::post('/',[ProductController::class,'store'])->name('store')
            ->middleware('auth');
//        Route::get('/create',[ProductController::class,'create'])->name('create');
        Route::get('/{id}',[UserController::class,'show'])->name('show');
//        Route::delete('/{id}',[ProductController::class,'destroy'])->name('delete')
//            ->middleware('auth');
//        Route::get('/{id}/edit',[ProductController::class,'edit'])->name('edit')
//            ->middleware('auth');
//        Route::put('/{id}',[ProductController::class,'update'])->name('update')
//            ->middleware('auth');
    });


