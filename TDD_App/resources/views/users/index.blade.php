
@extends('layouts.app')
@section('content')
    <div class="container">
{{--        <form class="form-inline" action="{{route('products.index')}}" method="GET">--}}
{{--            @csrf--}}
{{--            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="key" value="{{request(('key'))}}">--}}
{{--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
{{--        </form>--}}

        <h1>User Manager</h1>
        <a href=""  class="btn btn-success" style="margin-bottom: 10px">Create User</a>
        <div class="row justify-content-center">

            <table class="table table-striped">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>content</th>
                    <th>role</th>
                    <th>option</th>
                </tr>
                @foreach($user as $user)
                    @csrf
                    <tr>
                        <th>{{$user->id}}</th>
                        <th>{{$user->name}}</th>
                        <th>{{$user->email}}</th>
                        <th>1</th>
                        <th>
                            <a class="btn btn-success" href="{{route('users.show',$user->id)}}">view</a>
{{--                        @auth()--}}
{{--                            <form action="{{route('products.delete',$product->id)}}" method="POST">--}}
{{--                                @method('DELETE')--}}
{{--                                @csrf--}}
{{--                                <button class="btn btn-danger">delete</button>--}}
{{--                            </form>--}}
{{--                            <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}">edit</a>--}}
{{--                        @endauth--}}

                            <button class="btn btn-primary">edit</button>
                            <button class="btn btn-danger">delete</button>
                        </th>
                    </tr>
                @endforeach
            </table>

        </div>
{{--        {{$user->links() }}--}}
    </div>
@endsection
