
@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>User Manager</h1>
        <div class="row justify-content-center">
            <a class="btn btn-outline-primary" href="{{route('users.index')}}" style="margin-bottom: 10px">Back List User</a>
            <table class="table table-striped">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>mail</th>
                    <th>password</th>
                    <th>role</th>
                    <th>option</th>
                </tr>
                    @csrf
                    <tr>
                        <th>{{$user->id}}</th>
                        <th>{{$user->name}}</th>
                        <th>{{$user->email}}</th>
                        <th>{{$user->password}}</th>
                        <th>1</th>
                        <th>
                            {{--                            <a class="btn btn-success" href="{{route('products.show',$product->id)}}">view</a>--}}
                            {{--                        @auth()--}}
                            {{--                            <form action="{{route('products.delete',$product->id)}}" method="POST">--}}
                            {{--                                @method('DELETE')--}}
                            {{--                                @csrf--}}
                            {{--                                <button class="btn btn-danger">delete</button>--}}
                            {{--                            </form>--}}
                            {{--                            <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}">edit</a>--}}
                            {{--                        @endauth--}}

                            <button class="btn btn-primary">edit</button>
                            <button class="btn btn-danger">delete</button>
                        </th>
                    </tr>
            </table>
        </div>
    </div>
@endsection
