@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Product Insert</h2> <br/>
                <form action="{{route('products.store')}}" method="POST">
                    @csrf
                    <div class="card-header">
                        <input type="text" class="form-group" name="name" placeholder="insert name">
                        @error('name')
                        <span class="error text-danger" id="name-error" for="name">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="card-body">
                        <input type="text" class="form-group" name="content" placeholder="insert content">
                    </div>
                    <button class="btn btn-success">bam thu ma xem</button>
                </form>
            </div>
        </div>
    </div>
@endsection
