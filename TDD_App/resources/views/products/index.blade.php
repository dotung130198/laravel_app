@extends('layouts.app')
@section('content')
    <div class="container">
        <form class="form-inline" action="{{route('products.index')}}" method="GET">
            @csrf
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="key" value="{{request(('key'))}}">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
        <a href="{{route('products.create')}}"  class="btn btn-success">Create Products</a>

        <div class="row justify-content-center">
            <table class="table table-striped">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>content</th>
                    <th>option</th>
                </tr>
                @foreach($products as $product)
                    @csrf
                    <tr>
                        <th>{{$product->id}}</th>
                        <th>{{$product->name}}</th>
                        <th>{{$product->content}}</th>
                        <th>
                            <a class="btn btn-success" href="{{route('products.show',$product->id)}}">view</a>
{{--                            @auth()--}}
                            <form action="{{route('products.delete',$product->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger">delete</button>
                            </form>
                            <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}">edit</a>
{{--                            @endauth--}}
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
