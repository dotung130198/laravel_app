<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateNewUserTest extends TestCase
{
    /** @test */
    public function authenticated_user_create_new_user()
    {
        $this->actingAs(User::factory()->create());
        $user = User::factory()->make()->toArray();
        $response = $this->post(route('users.store'),$user);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users',$user);
    }
}
