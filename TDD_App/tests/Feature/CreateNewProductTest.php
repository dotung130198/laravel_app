<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_create_new_product()
    {
        $this->actingAs(User::factory()->create());
        $this->withoutExceptionHandling();
        $product = Product::factory()->make()->toArray();
        $response = $this->post(route('products.store',$product));

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('products',$product);
        $response->assertRedirect(route('products.index'));
    }
    /** @test */
    public function unauthenticated_user_can_not_create_new_product()
    {
//
        $product = Product::factory()->make()->toArray();
        $response = $this->post(route('products.store',$product));

        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticated_user_create_new_product_not_null_name()
    {

        $this->actingAs(User::factory()->create());
        $product = Product::factory()->make(['name'=>null])->toArray();
        $response = $this->post(route('products.store',$product));

        $response->assertSessionHasErrors(['name']);
        $response->assertStatus(Response::HTTP_FOUND);


    }
     /** @test */
    public function authenticated_user_view_form_create_new_product()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $response = $this->get(route('products.create'));
        $response->assertViewIs('products.create');
    }
    /** @test */
    public function authenticated_user_can_see_error_not_null_name()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->make(['name'=>null])->toArray();
        $response = $this->from(route('products.create'))->post(route('products.store',$product));

        $response->assertRedirect(route('products.create'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_must_login_can_see_form_create()
    {
        $product = Product::factory()->make(['name'=>null])->toArray();
        $response = $this->from(route('products.create'))->post(route('products.store',$product));

        $response->assertRedirect('/login');
    }




}
