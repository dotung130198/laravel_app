<?php

namespace Tests\Feature;

use App\Models\Product;

use Tests\TestCase;

class GetProductTest extends TestCase
{
    /** @test */
    public function get_all_list_product()
    {
        $this->withoutExceptionHandling();
        $products = Product::factory()->create();
        $response = $this->get(route('products.index'));

        $response->assertStatus(200);
        $response->assertViewIs('products.index');
        $response->assertSee(['name']);
    }

    /** @test */
    public function get_one_product()
    {
        $this->withoutExceptionHandling();
        $product = Product::factory()->create();
        $response = $this->get(route('products.show',$product->id));

        $response->assertStatus(200);
        $response->assertViewIs('products.show');
        $response->assertSee(['name']);
    }
}
