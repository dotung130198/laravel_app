<?php

namespace Tests\Feature;


use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use function Symfony\Component\Translation\t;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_product()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $product =Product::factory()->create();
        $response = $this->get(route('products.edit',$product->id));

        $response->assertStatus(200);
        $response->assertViewIs('products.edit');
        $response->assertSee(['name']);
    }
    /** @test */
    public function unauthenticated_user_not_update_product()
    {
//        $this->withoutExceptionHandling();
        $product =Product::factory()->create();
        $response = $this->get(route('products.edit',$product->id));

        $response->assertRedirect('/login');

    }
    /** @test */
    public function authenticated_user_save_update_product()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $product =Product::factory()->create();
        $product['name']='Name (update)';
        $response = $this->put(route('products.update',$product->id),$product->toArray());

        $this->assertDatabaseHas('products',['id'=>$product->id,'name'=>'Name (update)']);
        $response->assertRedirect(route('products.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function unauthenticated_user_not_save_update_product()
    {
//        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $product =Product::factory()->create();
        $product['name']='Name (update)';
        $response = $this->put(route('products.update',$product->id),$product->toArray());

//        $this->assertDatabaseHas('products',['id'=>$product->id,'name'=>'Name (update)']);
        $response->assertRedirect(route('products.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

}
