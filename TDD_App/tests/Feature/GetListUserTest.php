<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListUserTest extends TestCase
{
    /** @test */
    public function get_all_list_all_user()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $response = $this->get(route('users.index'));

        $response->assertStatus(200);
        $response->assertViewIs('users.index');
        $response->assertSee(['name']);;
    }
    /** @test */
    public function get_one_user()
    {
//        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $response = $this->get(route('users.show',$user->id));

        $response->assertStatus(200);
        $response->assertViewIs('users.show');
        $response->assertSee(['name']);;
    }

}
