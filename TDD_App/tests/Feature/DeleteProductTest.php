<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_delete_products()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $product=Product::factory()->create();
        $response = $this->delete(route('products.delete',$product->id));


        $this->assertDatabaseMissing('products',$product->toArray());
        $response->assertRedirect(route('products.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function unauthenticated_user_can_delete_products()
    {
        $product=Product::factory()->create();
        $response = $this->delete(route('products.delete',$product->id));

        $response->assertRedirect('/login');
    }
}
